==========================================
Documentation de préinstallation Exolegend
==========================================

**Bienvenue à l'Exolegend !** 

Avant d'avancer plus loin, il est **impératif** de rejoindre notre serveur Discord.
Toutes les informations seront partagées sur le Discord pendant la compétition. 
Vous aurez aussi besoin du discord pour intéragir avec les arènes et procéder aux
préinscriptions.

    - Lien du Discord : https://discord.gg/zjaRjggTqf

Vous trouverez sur ce repo les instructions nécessaires pour mettre
en place votre environnement de travail.

Juste au dessus, sélectionnez votre système d'exploitation (**LINUX.rst** si vous êtes sous Linux ou **WINDOWS.rst** si vous êtes sous Windows)
et suivez les instructions.


=====
LINUX
=====

https://gitlab.com/exolegend/preinstall/-/blob/main/LINUX.rst

=======
WINDOWS
=======

https://gitlab.com/exolegend/preinstall/-/blob/main/WINDOWS.rst



Si vous rencontrez des diffcultés lors de l'installation, ne pas hésiter
à envoyer un message sur notre Discord.

Bon courage à tous ;)

L'équipe Exolegend