.. role:: red
    :class: red-text

.. role:: green
    :class: green-text

.. raw:: html

    <style>
        .red-text {
            color: red;
            font-size: 20px;
        }
        .green-text {
            color: green;
        }
    </style>


.. |vscode_link| replace:: https://code.visualstudio.com

.. |virtuals_linux_link| replace:: https://we.tl/t-yn5KlpOOwT
.. |virtuals_win_link| replace:: https://we.tl/t-56Jx9ql2gn
    
.. |msys2_link| replace:: https://www.msys2.org/



==================================================
Documentation de préinstallation Exolegend (LINUX)
==================================================

Installation de Visual Studio Code
----------------------------------

| Installer Visual Studio Code en suivant le lien suivant : |vscode_link|
| Ou en tapant la commande suivante:

.. code-block:: bash

    snap install --classic code

Installation des outils nécessaires
-----------------------------------

Installation de python3 et cpulimit:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: bash
    
    sudo apt install cpulimit python3-venv

Installation de node:
^^^^^^^^^^^^^^^^^^^^^

.. code-block:: bash
    
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.5/install.sh | bash
    nvm install node
    export NVM_DIR="$HOME/.nvm"                                                    
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
    [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

    npm install -g serve

Installation des librairies externes:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: bash
    
    sudo apt install -y libedit-dev make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev git

Autoriser la lecture et l'écriture sur le port UART
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Taper les commandes suivantes:

.. code-block:: bash
    
    sudo usermod -a -G dialout $USER
    sudo reboot

Installation de PlatformIO
--------------------------

Ouvrir Visual Studio Code :
    :red:`1.` Cliquer sur l'ongler **Extension**

    :red:`2.` Ecrire dans la barre de recherche " PlatformIO IDE "

    :red:`3.` CLiquer sur **INSTALL**

    :red:`4.` Une fois fini **REDEMARRER VISUAL STUDIO CODE** pour finaliser l’installation, cliquer sur l'icône de platform sur le menu à gauche de la fenêtre

    :red:`5.` Cliquer sur INSTALL PLATFORMIO CORE proposé

.. image:: images/install_platformio.png
    :align: center

Téléchargement de Virtuals (l'outil de simulation)
--------------------------------------------------

Lien de Téléchargement : |virtuals_linux_link|
