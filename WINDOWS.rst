.. role:: red
    :class: red-text

.. role:: green
    :class: green-text

.. raw:: html

    <style>
        .red-text {
            color: red;
            font-size: 20px;
        }
        .green-text {
            color: green;
        }
    </style>


.. |vscode_link| replace:: https://code.visualstudio.com

.. |virtuals_linux_link| replace:: https://we.tl/t-yn5KlpOOwT
.. |virtuals_win_link| replace:: https://we.tl/t-56Jx9ql2gn
    
.. |msys2_link| replace:: https://www.msys2.org/


    
====================================================
Documentation de préinstallation Exolegend (WINDOWS)
====================================================

Installation de Visual Studio Code
----------------------------------

| Installez Visual Studio Code en suivant le lien suivant : |vscode_link|

Installation des outils nécessaires
-----------------------------------

Installation de node:
^^^^^^^^^^^^^^^^^^^^^

Vous pouvez utiliser ``fnm`` pour installer Node.js et npm. 
``fnm`` est un gestionnaire de versions Node.js rapide et fiable.

.. code-block:: bash
    
    # Download and install fnm:
    winget install Schniz.fnm
    # Download and install Node.js:
    fnm install 22


Ou bien vous pouvez installer Node.js et npm en suivant ce lien en passant par le site officiel et en téléchargeant ``Windows installer (.msi)```

NodeJs download: https://nodejs.org/en/download

Attention, bien sélectionner `for Windows` si ce n'est pas le cas

Une fois node installé, il est possible de vérifier l'installation en lançant les commandes suivantes :

.. code-block:: bash

    node --version
    npm --version

Si vous n'avez pas d'erreur, vous pouvez installer serve:

.. code-block:: bash

    npm install -g serve


Installation du compilateur MSYS2:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    Installer MSYS2 en suivant ce lien : |msys2_link|

    Cliquer sur le lien de téléchargement

    .. image:: images/msys2_link.png
        :align: center
    
    Lorsque le téléchargement se fini avec succès, lancer l'installateur 
    et bien sélectionner **Run MSYS2 now** avant de cliquer sur "Finish"

    .. image:: images/msys2_installer.png
        :align: center
    
    La console suivante se lance :

    .. image:: images/msys2_console.png
        :scale: 50%
        :align: center

    Dans la console lancer la commande suivante:

    .. code-block:: bash

        pacman -S mingw-w64-ucrt-x86_64-gcc
    
    Après l'installation il est possible de vérifier l'installation en lançant la commande : 

    .. code-block:: bash

        gcc --version
    
    Si le résultat de la commande : ``gcc.exe (Rev1, Built by MSYS2 project) xx.x.x`` 
        => :green:`OK pour quitter`
    
Ajouter la toolchain de MSYS2 dans la variable d'environnement
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    Il faut ajouter ``C:\msys64\usr\bin`` et ``C:\msys64\ucrt64\bin`` dans la variable d'environnement ``PATH``
    de Windows :
    
    1. Taper **path** dans le menu Windows :

    .. image:: images/msys2_path.png
        :align: center
    

Installation de Microsoft Visual C++ (msvc) :
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Installer Microsoft Visual C++ (2015, 2017, 2019 et 2022) en suivant ce lien : https://aka.ms/vs/17/release/vc_redist.x64.exe

Installation du driver ESP32 :
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    * Télécharger le driver Pololu CP2102 en suivant le lien : https://www.pololu.com/file/0J14/pololu-cp2102-windows-220616.zip
    * Dézipper le fichier téléchargé
    * Executer l'installateur:
        * Si système d'exploitation 64 bits, exécuter **pololu-cp2102-setup-x64.exe**.
        * Si système d'exploitation 32 bits, exécuter **pololu-cp2102-setup-x86.exe**.
    * Suivre les instructions dans la fenêtre d'installation
    * Lorsque l'installation se termine avec succès fermer la fenêtre

Installation de PlatformIO
--------------------------

Ouvrir Visual Studio Code :
    :red:`1.` Cliquer sur l'ongler **Extension**

    :red:`2.` Ecrire dans la barre de recherche " PlatformIO IDE "

    :red:`3.` CLiquer sur **INSTALL**

    :red:`4.` Une fois fini **REDEMARRER VISUAL STUDIO CODE** pour finaliser l’installation, cliquer sur l'icône de platform sur le menu à gauche de la fenêtre

    :red:`5.` Cliquer sur INSTALL PLATFORMIO CORE proposé


.. image:: images/install_platformio.png
    :align: center

Téléchargement de Virtuals (l'outil de simulation)
--------------------------------------------------

Lien de Téléchargement : |virtuals_win_link|




